function UpdateDic()
{
	var item = document.getElementById('div_content');
	var backPage = chrome.extension.getBackgroundPage();
	var map = backPage.mapResult;
	var mapOrder = backPage.mapResultOrder;
	var itemHtml = "<table>";
	for (var i in mapOrder)
	{
		itemHtml += "<tr>";
		itemHtml += "<td>";
		itemHtml += mapOrder[i];
		itemHtml += "</td>";
		itemHtml += "</tr>";
	}
	itemHtml += "</table>";
	item.innerHTML = itemHtml;
	
	var rows = item.getElementsByTagName("tr");
	for (i = 0; i < rows.length; i++) {
        var currentRow = rows[i];
        var createClickHandler = 
            function(row) 
            {
                return function() { 
                                        var cell = row.getElementsByTagName("td")[0];
                                        var id = cell.innerHTML;
                                        //alert("id:" + id);
										backPage.TryGetAgain(id);
                                 };
            };

        currentRow.onclick = createClickHandler(currentRow);
    }
}

window.onload = UpdateDic;