/**
 * Copyright (c) 2011 The Chromium Authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */
var lastUrl = '';
var lastUtterance = '';
var speaking = false;
var globalUtteranceIndex = 0;

var mapInput = {};
var mapResult = {};
var mapResultOrder = [];

function speak1(utterance) {
	lastUtterance = utterance;
	globalUtteranceIndex++;
	var utteranceIndex = globalUtteranceIndex;

	var rate = localStorage['rate'] || 1.0;
	var pitch = localStorage['pitch'] || 1.0;
	var volume = localStorage['volume'] || 1.0;
	var voice = localStorage['voice'];
	chrome.tts.speak(
			utterance,
			{
				voiceName: voice,
				rate: parseFloat(rate),
				pitch: parseFloat(pitch),
				volume: parseFloat(volume),
				lang: 'en-US',
				onEvent: function(evt) {
					if (evt.type == 'end' ||
						evt.type == 'interrupted' ||
						evt.type == 'cancelled' ||
						evt.type == 'error') {
						if (utteranceIndex == globalUtteranceIndex) {
							speaking = false;
					}
				}
				}
			}
		);
}

function TryGet(utterance) {
	if (utterance == '') {
		console.log('null string');
		return;
	}

	if (utterance in mapInput)
	{
		utterance = mapInput[utterance];
	}
	if (utterance in mapResult)
	{
		var dicResult = mapResult[utterance];
		showNotify(utterance, dicResult);
		var index = mapResultOrder.indexOf(utterance);
		if (index > -1)
		{
			mapResultOrder.splice(index, 1);
			mapResultOrder.unshift(utterance);
		}
	}
	else
	{
		connectDic(utterance);
	}
	speak1(utterance);
}

function TryGetAgain(word)
{
	if (word in mapResult)
	{
		showNotify(word, mapResult[word]);
	}
	speak1(word);
}

function connectDic(msg) {
	var xmlhttp = new XMLHttpRequest();
	var url = GetYaUrl(msg);
	lastUrl = url;
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			parseDic(msg, xmlhttp.responseText);
		}
	};
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}

function GetYaUrl(word)
{
	return "https://tw.dictionary.search.yahoo.com/search?p=" + word + "&fr2=dict";
}

function parseDic(msg, body) {
	var div = document.createElement("html");
	div.innerHTML = body;
	
	var nodes = div.getElementsByClassName("searchCenterMiddle");
	div.innerHTML = nodes[0].innerHTML;
	
	var nodes = div.getElementsByClassName("compArticleList");
	var array = "";
	
	var nodesTitle = div.getElementsByClassName("compTitle");
	if (nodesTitle.length > 0)
	{
		var titles = nodesTitle[0].innerText.split(" ");
		if (titles.length > 0 && titles[0].length > 0)
		{
			if (msg !== titles[0])
			{
				if (!(msg in mapInput))
				{
					mapInput[msg] = titles[0];
				}
				msg = titles[0];
			}
		}
	}
	
	if (nodes.length == 0) {
		showNotify(msg, "not found");
	} else {
		var arrayHtml = "";
		for(var i=0; i<nodes.length; i++) {
			array += nodes[i].innerText;
			arrayHtml += nodes[i].innerHTML;
			if (i > 0)
				break;
			array += "\n";
		}
		mapResult[msg] = array;
		mapResultOrder.unshift(msg);
		if (mapResultOrder.length > 10)
		{
			var lastItem = mapResultOrder.pop();
			delete mapResult[lastItem];
		}
		showNotify(msg, array);
	}
}

function showNotify(msg, body) {
	lastUrl = GetYaUrl(msg);
    chrome.notifications.clear(
		'id1',
		function() {} 
	);
	chrome.notifications.create(
		'id1',
		{   
			type:"basic",
			title:msg,
			message:body,
			iconUrl:"icon-128.png"
		},
		function() {} 
	);
}   

function initBackground() {
	chrome.contextMenus.create(
		{
			id: "LittleDic",
			title: "Search Little-Dictionary for the selected",
			contexts: ["selection"]
		}
	);
	chrome.contextMenus.onClicked.addListener(
		function(sel) {
			if (sel.selectionText)
				TryGet(sel.selectionText);
		}
	);
	chrome.notifications.onClicked.addListener(
		function(notificationId, byUser) {
			if (lastUrl != '') {
				chrome.tabs.create({url: lastUrl});
				chrome.notifications.clear(
					'id1',
					function() {} 
				);
			}
		}
	);
}

initBackground();
